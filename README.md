Scripts to be able to grab all data from the Seloc Tech Wiki.

Prelimary steps :

1. Must be using Mac OSX.
    ImageOptim only works on Mac OSX and the script uses it to optimize images for use in the app.
2. Open ProjectProperties.groovy and update the baseDirPath to point to the location of where you checked out the project
3. (Optional) All depending on your operating system you may need to add 'git config core.ignorecase false' since the final-content directory requires files to be lower case.


Running the script to generate all html and download all images :

1. Execute DoAllTheThings.groovy found in src/main/java
    * May require memory settings to be able to run successfully (ex. -Xmx1024m)
2. The output at the end of the script will give you manual fixes that will need to be fixed before adding the content to the android app


