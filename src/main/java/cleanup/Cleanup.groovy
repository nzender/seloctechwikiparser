package cleanup

import groovy.io.FileType
import util.ProjectProperties

ProjectProperties.finalContentDir.eachFileRecurse(FileType.FILES, {file->
    file.delete()
})

ProjectProperties.outputDirectoriesForTypes.each{outputDir->
    if(outputDir.exists()){
        outputDir.eachFileRecurse(FileType.FILES, {file->
            file.delete()
        })
    }
}