package android.navigation

import groovy.json.JsonBuilder
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import util.ProjectProperties

def prettyCategoryNames = [
    "categories" : "Articles By Category",
    "cartypes" : "Articles By Car Type",
    "all" : "All Articles"
]

String baseDir = ProjectProperties.baseDirPath

def types = ProjectProperties.types.clone()
types.removeAll {
    it == ProjectProperties.missingType
}

def json = [categories: []]
int index = 1;

types.each { type ->
    println ">>>>>>>>>>>>> Processing all files of type ${type} >>>>>>>>>>>>>>"

    def category = [name: prettyCategoryNames[type], id: type, children: []]
    json.categories << category

    FileUtils.getFile(baseDir, ProjectProperties.resourcesDirPart, type).eachFile { file ->
        if (file.isFile() && !file.isHidden()) {
            def categoryChild = [id : index, name: FilenameUtils.removeExtension(file.name).replaceAll("_", " "), children: []]
            index ++;
            category.children << categoryChild

            println "------------- ${file.name} ----------------"
            def xml = new XmlSlurper().parse(file)
            xml.page.each {
                def page = [title: it.title.text(), pageId: it.id.text()]
                categoryChild.children << page
            }
        }
    }
}


def allPages = []
json.categories.each{category->
    category.children.each{pages->
        pages.children.sort{it.title}
    }
    allPages.addAll(category.children)
    category.children.sort{it.name}
}

allPages.sort{it.name}
json.categories << [name: prettyCategoryNames["all"], id: "all", children: allPages]

ProjectProperties.getAndroidNavigationJson().parentFile.mkdirs()
ProjectProperties.getAndroidNavigationJson().text = new JsonBuilder(json).toString()