package download

import groovyx.gpars.GParsPool
import org.apache.commons.io.FileUtils
import util.ProjectProperties


List<File> outputDirByType = ProjectProperties.outputDirectoriesForTypes


def download(address, File baseDir, fileName){
    File fileOnDisk = FileUtils.getFile(baseDir, fileName)
    if(fileOnDisk.exists() && fileOnDisk.size() > 0){
        println "Already downloaded file with name ${fileName}"
        return true
    }
    def file = new FileOutputStream(fileOnDisk)
    def out = new BufferedOutputStream(file)
    boolean result = false;
    try{
        URL url = new URL(address)
        out << url.openStream()
        result = true;
    }catch(FileNotFoundException e){
        println "!!!!! file not found at address ${address}"
    } catch(UnknownHostException e ){
        println "!!!!! does this website still exists or do you have an internet connection??!?! ${address}"
    }finally {
        file.close()
        out.close()
    }
    if(!result){
        fileOnDisk.delete()
    }
    result
}

outputDirByType.each{outputDir ->
    println ">>>>>>> PROCESSING ${outputDir.path}"
    def fileLines = []
    File csv = FileUtils.getFile(outputDir, ProjectProperties.fileRefToUrlCSV)
    if(!csv.exists()){
        println "!!!! WARN : ${csv.path} does not exist...moving on"
        return
    }

    csv.eachLine{
        fileLines << it
    }

    GParsPool.withPool {
        fileLines.eachParallel {
            File imageDir = FileUtils.getFile(outputDir, ProjectProperties.imageDirPart)
            imageDir.mkdir()

            def name = it.split(",")

            //Remove anything that starts with File: or Image:
            def title = name[0]
            if(title.contains(":")){
                title = title.split(":")[1]
            }
            def address = name[1]
            def image = FileUtils.getFile(imageDir, title)
            if(image.exists()){
                println "!! ${image.name} already exists"
            }else{
                println ">> Downloading ${image.name}"
                download(address, imageDir, title)
            }
        }
    }
}