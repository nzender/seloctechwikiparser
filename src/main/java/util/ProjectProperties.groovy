package util

import org.apache.commons.io.FileUtils

class ProjectProperties {
    private ProjectProperties(){}

    /**
     * If running on a different box this property should be the only one that is needed to be updated to point
     * to the location of the project on your machine
     */
    public static final String baseDirPath = "/Users/zender/dev/projects/selocTechWikiMobileApp/SelocTechWikiParser"


    public static final String wikiBaseUrl = "http://wiki.seloc.org"

    public static final String categoriesType = "categories"
    public static final String carTypes = "cartypes"
    public static final String missingType = "missing"
    public static final String fileRefToCsvGenerator = "fileRefToUrlCsvGenerator.html"
    public static final String fileRefToUrlCSV = "file_ref_to_url.csv";
    public static final String finalContentDirPart = "final-content"
    public static final String outputDirPart = "output"
    public static final String imageDirPart = "images"
    public static final String  resourcesDirPart = "src/main/resources"
    public static final String androidNavigationJsonPart = "navigation/full.json"
    public static final List<String> types = [categoriesType, carTypes, missingType]
    public static final String allTemplatesXml = "TechWiki-All-Templates.xml"
    public static final String templatesDirPart = "templates"

    public static File getMissingOutputDirectory(){
        FileUtils.getFile(resourcesDir, missingType, outputDirPart)
    }

    public static File getAllTemplatesXmlFile(){
        FileUtils.getFile(resourcesDir, templatesDirPart, allTemplatesXml)
    }

    public static File getBaseDir(){
        FileUtils.getFile(baseDirPath);
    }

    public static File getFinalContentDir(){
        FileUtils.getFile(baseDir, finalContentDirPart)
    }

    public static File getAndroidNavigationJson(){
        FileUtils.getFile(finalContentDir, androidNavigationJsonPart)
    }

    public static List<File> getImageDirectoriesForTypes(){
        def files = []
        types.each{type ->
            files << FileUtils.getFile(resourcesDir, type, outputDirPart, imageDirPart)
        }
        files
    }

    public static List<File> getOutputDirectoriesForTypes(){
        def files = []
        types.each{type ->
            files << FileUtils.getFile(resourcesDir, type, outputDirPart)
        }
        files
    }

    public static File getResourcesDir(){
        FileUtils.getFile(baseDir, resourcesDirPart)
    }

    public static List<File> getTopLevelDirectoriesForTypes(){
        def files = []
        types.each{type ->
            files << FileUtils.getFile(resourcesDir, type)
        }
        files
    }

}
