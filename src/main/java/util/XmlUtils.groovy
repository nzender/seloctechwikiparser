package util

import org.codehaus.groovy.runtime.StringGroovyMethods

class XmlUtils {

    /**
     * The xml files are already escaped in some places but not other. Only doing 3 of the normal
     * 5 entities you need to properly escape xml to fix some issues.
     *
     * Quotes and Apostrophes have been the 2 entities left out of this escape method.
     *
     * @param orig
     * @return
     */
    public static String partialXmlEscape(String orig) {
        return StringGroovyMethods.collectReplacements(orig, new Closure<String>(null) {
            public String doCall(Character arg) {
                switch (arg) {
                    case '&':
                        return "&amp;";
                    case '<':
                        return "&lt;";
                    case '>':
                        return "&gt;";
                }
                return null;
            }
        });
    }
}
