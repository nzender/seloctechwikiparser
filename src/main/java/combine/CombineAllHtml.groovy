package combine

import org.apache.commons.io.FileUtils
import util.ProjectProperties

File finalDir = ProjectProperties.finalContentDir
List<File> typeFiles = ProjectProperties.topLevelDirectoriesForTypes

def fileNames = []

typeFiles.each{typeDir->
    typeDir.eachFileRecurse {file->
        if(file.name.endsWith("html") && file.name != ProjectProperties.fileRefToCsvGenerator && !file.hidden){
            if(fileNames.contains(file.name)){
                println ">>>>> SKIPPING : ${file.name} already exists"
            }else{
                println "!!!!!! Moved : ${file.name}"
                fileNames << file.name

                FileUtils.copyFileToDirectory(file, finalDir)
            }
        }
    }
}
