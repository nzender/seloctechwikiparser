package combine

import org.apache.commons.io.FileUtils
import util.ProjectProperties

File finalDir = ProjectProperties.finalContentDir
List<File> typeImageFiles = ProjectProperties.imageDirectoriesForTypes

def fileNames = []
typeImageFiles.each{dir->
    dir.eachFileRecurse {file->
        if(file.isFile()){
            if(fileNames.contains(file.name)){
                println ">>>>> SKIPPING : ${file.name} already exists"
            }else{
                println "!!!!!! Copied : ${file.name}"
                fileNames << file.name

                FileUtils.copyFileToDirectory(file, finalDir)
            }
        }
    }
}