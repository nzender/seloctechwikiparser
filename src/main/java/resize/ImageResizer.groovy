package resize

import groovyx.gpars.GParsPool
import org.apache.commons.io.FilenameUtils
import org.imgscalr.Scalr
import util.ProjectProperties

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

File baseDir = ProjectProperties.finalContentDir

def filesToProcess = []
baseDir.eachFileRecurse {file->
    if(!file.name.endsWith("html") && !file.name.endsWith("json") && !file.isHidden() && file.isFile()){
        filesToProcess << file.path
    }
}

GParsPool.withPool{
    filesToProcess.eachParallel {filePath->
        def file = new File(filePath)
        println "------------ ${file.name} ---------------"
        try{
            BufferedImage image = ImageIO.read(file);
            if(image.getHeight() > 700 || image.getWidth() > 700){
                BufferedImage resizedImage = Scalr.resize(image, 700)
                println ">>>>>> Resizing image at path : ${file.path}"
                ImageIO.write(resizedImage, FilenameUtils.getExtension(file.name), file);
            }else{
                println ">>>>>> File not big enough moving on"
            }
        }catch(e){
            println "!!!!!!!!!!!! ERROR : Something happened when trying to process file ${e.message}"
        }
    }
}