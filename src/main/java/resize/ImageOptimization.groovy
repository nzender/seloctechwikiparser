package resize

import util.ProjectProperties

def command = "${ProjectProperties.baseDirPath}/src/main/util/ImageOptim.app/Contents/MacOS/ImageOptim ${ProjectProperties.finalContentDir.path}"
def proc = command.execute()
proc.waitFor()

println "----- image optimization complete ------"
println "return code: ${ proc.exitValue()}"
println "stderr: ${proc.err.text}"
println "stdout: ${proc.in.text}"