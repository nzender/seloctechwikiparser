package resize

import com.googlecode.htmlcompressor.compressor.HtmlCompressor
import util.ProjectProperties

File baseDir = ProjectProperties.finalContentDir

baseDir.eachFileRecurse {file->
    if(file.name.endsWith("html") && !file.hidden){
        println "--------------- ${file.name} ----------------"
        String html = file.getText()
        HtmlCompressor compressor = new HtmlCompressor();
        compressor.setGenerateStatistics(true);
        String compressedHtml = compressor.compress(html);

        println(String.format(
                "Compression time: %,d ms, Original size: %,d bytes, Compressed size: %,d bytes",
                compressor.getStatistics().getTime(),
                compressor.getStatistics().getOriginalMetrics().getFilesize(),
                compressor.getStatistics().getCompressedMetrics().getFilesize()
        ));

        file.text = compressedHtml
    }
}
