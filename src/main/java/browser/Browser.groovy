package browser

import org.apache.commons.io.FileUtils
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.phantomjs.PhantomJSDriver
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.WebDriverWait
import util.ProjectProperties

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfAllElementsLocatedBy
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated

public class Browser {
    private static final int DEFAULT_WAIT_TIME = 10
    RemoteWebDriver driver

    public Browser() {
        System.setProperty("phantomjs.binary.path", FileUtils.getFile(ProjectProperties.baseDirPath, "src", "main", "util", "phantomjs").path)
        driver = new PhantomJSDriver()

//        System.setProperty("webdriver.chrome.driver", "/Users/zender/dev/software/chrome-driver-selenium/2.9/chromedriver")
//        driver = new ChromeDriver()
    }

    public void close() {
        driver.close()
    }

    public Browser goTo(String url) {
        driver.get(url)
        this
    }

    public List<WebElement> getElements(By elementSelector, int waitTime = DEFAULT_WAIT_TIME) {
        new WebDriverWait(driver, waitTime)
                .until(presenceOfAllElementsLocatedBy(elementSelector));
    }

    public WebElement getElement(By elementSelector, int waitTime = DEFAULT_WAIT_TIME) {
        new WebDriverWait(driver, waitTime)
                .until(presenceOfElementLocated(elementSelector));
    }

    public def waitFor(ExpectedCondition condition, int waitTime = DEFAULT_WAIT_TIME) {
        new WebDriverWait(driver, waitTime)
                .until(condition);
    }

    public def selectElement(WebElement element){
        if(element.selected){
            element
        }else{
            element.click()
        }
    }

    public def deselectElement(WebElement element){
        if(!element.selected){
            element
        }else{
            element.click()
        }
    }
}
