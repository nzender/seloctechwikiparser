package browser
import org.apache.commons.io.FileUtils
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import util.ProjectProperties

import static org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElement

Browser browser = new Browser()

args.each{
    File outputDir = FileUtils.getFile(ProjectProperties.resourcesDir, it, ProjectProperties.outputDirPart)
    browser.goTo("file:///${FileUtils.getFile(outputDir, ProjectProperties.fileRefToCsvGenerator).path}")

    WebElement doIt = browser.getElement(By.id("doIt"))
    WebElement doneYet = browser.getElement(By.id("doneYet"))
    WebElement output = browser.getElement(By.id("output"))

    doIt.click()

    browser.waitFor(textToBePresentInElement(doneYet, "YES"));

    //Adding sleep here since selenium works so fast that the dom is not done drawing all elements yet
    sleep(2000)
    FileUtils.getFile(outputDir, ProjectProperties.fileRefToUrlCSV).text = output.text
}

println "Closing browser...."
browser.close()
println "Done"