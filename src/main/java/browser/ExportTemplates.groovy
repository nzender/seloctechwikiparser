package browser

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import util.ProjectProperties

Browser browser = new Browser()
browser.goTo("${ProjectProperties.wikiBaseUrl}/index.php?title=Special:MostLinkedTemplates&limit=500&offset=0")

List<WebElement> elements = browser.getElements(By.cssSelector("li a[title^='Template:']"))

def templatePageNames = []
elements.each{ WebElement element ->
    String title = element.getAttribute("title")
    if(!title.contains("page does not exist")){
        templatePageNames << title
    }
}

browser.goTo("${ProjectProperties.wikiBaseUrl}/a/Special:Export/")

WebElement pagesTextArea = browser.getElement(By.name("pages"))

pagesTextArea.sendKeys(templatePageNames.join("\n"))

String result = "";

if(pagesTextArea.getAttribute("value")){
    WebElement currentRevisionOnly = browser.getElement(By.name("curonly"))
    WebElement includeTemplates = browser.getElement(By.name("templates"))
    WebElement saveToFile = browser.getElement(By.name("wpDownload"))

    browser.selectElement(currentRevisionOnly)
    browser.deselectElement(includeTemplates)
    browser.deselectElement(saveToFile)

    if(currentRevisionOnly.isSelected() && !saveToFile.isSelected() && !includeTemplates.isSelected()){
        WebElement exportButton = browser.getElement(By.cssSelector("[value='Export']"));

        println pagesTextArea.getAttribute("value")

        exportButton.click()

        result = browser.driver.getPageSource()

    }else{
        println "!!!!! ERROR checkboxes not selected correctly"
    }

}else{
    println "!!!! ERROR No text put into box"
}

if(result){
    ProjectProperties.allTemplatesXmlFile.text = result
}else{
    println "!!!!!! ERROR No template xml returned"
}

println "Closing browser...."
browser.close()
println "Done"