package browser
import org.apache.commons.io.FileUtils
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import util.ProjectProperties

def isOfCarTypeCategory = {title ->
    title.contains("Lotus") || title.contains("S1") || title.contains("S2") || title.contains("VX220") || title.contains("Exige")
}


Browser browser = new Browser()
browser.goTo("${ProjectProperties.wikiBaseUrl}/a/SELOC_TechWiki")

def carTypeCategories = []
def otherCategories = []
List<WebElement> elements = browser.getElements(By.cssSelector("b a[title^='Category:']"))
elements.each{ WebElement element ->
    String title = element.getAttribute("title")
    if(isOfCarTypeCategory(title)){
        carTypeCategories << title
    }else{
        otherCategories << title
    }
}
otherCategories = otherCategories.unique()
carTypeCategories = carTypeCategories.unique()

carTypeCategories.each{String category->
    String xml = downloadXml(browser, category)
    category = cleanCategoryName(category)
    FileUtils.getFile(ProjectProperties.getResourcesDir(), ProjectProperties.carTypes, "${category}.xml").text = xml
}

otherCategories.each{String category->
    String xml = downloadXml(browser, category)
    category = cleanCategoryName(category)
    FileUtils.getFile(ProjectProperties.getResourcesDir(), ProjectProperties.categoriesType, "${category}.xml").text = xml
}

println "Closing browser...."
browser.close()
println "Done"

private String downloadXml(Browser browser, String category){
    browser.goTo("${ProjectProperties.wikiBaseUrl}/a/Special:Export/")

    WebElement categoryName = browser.getElement(By.id("catname"))
    WebElement addCategory = browser.getElement(By.name("addcat"))

    categoryName.sendKeys(category)

    addCategory.click()

    WebElement pages = browser.getElement(By.name("pages"))
    println pages.text


    WebElement currentRevisionOnly = browser.getElement(By.name("curonly"))
    WebElement includeTemplates = browser.getElement(By.name("templates"))
    WebElement saveToFile = browser.getElement(By.name("wpDownload"))

    String result = "";

    if(pages.text){

        browser.selectElement(currentRevisionOnly)
        browser.deselectElement(saveToFile)
        browser.deselectElement(includeTemplates)

        if(currentRevisionOnly.isSelected() && !saveToFile.isSelected() && !includeTemplates.isSelected()){
            WebElement exportButton = browser.getElement(By.cssSelector("[value='Export']"));

            exportButton.click()

            result = browser.driver.getPageSource()
        }else{
            println "!!!!! ERROR checkboxes not selected correctly"
        }
    }else{
        println "!!!!! ERROR no pages found for the category : ${category}"
    }
    result
}

private String cleanCategoryName(String category) {
    category = category.replaceAll(" ", "_").replace("Category:", "")
    category
}