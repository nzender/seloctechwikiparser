package traverse

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import templates.HtmlTemplates
import util.ProjectProperties

def allItems = []
def allExistingImages = []

ProjectProperties.imageDirectoriesForTypes.each{imageDir->
    if(imageDir.exists()){
        imageDir.eachFileRecurse {
            allExistingImages << it.name.toLowerCase()
        }
    }
}

ProjectProperties.topLevelDirectoriesForTypes.each{dir->
    dir.eachFileRecurse{ file->
        if(file.name.endsWith("html") && file.name != ProjectProperties.fileRefToCsvGenerator && !file.hidden){
            Document doc = Jsoup.parse(file.getText());
            List<Element> images = doc.select("img")
            images.each{image->

                if(!allExistingImages.contains(image.attr("src")?.toLowerCase())){
                    println "---------------- ${file.name} ----------------"
                    println "!!!!!NOT FOUND ${image.attr("src")}"
                    def imageUrl = image.parent().attr("href")
                    def imageOnly = imageUrl.replace("${ProjectProperties.wikiBaseUrl}/a/", "")
                    //println imageOnly
                    allItems << imageOnly
                }
            }
        }
    }
}

allItems.unique()

println "Items found : ${allItems.size()}"
def itemsHtml = HtmlTemplates.renderListOfItemsInChunksOfHtml(allItems)

def missingImagesDir = ProjectProperties.missingOutputDirectory
if(!missingImagesDir.exists()){
    missingImagesDir.parentFile.mkdir()
    missingImagesDir.mkdir()
}

def html = HtmlTemplates.renderFileRefToUrlCsvGenerator([filesToLookup: itemsHtml, wikiBaseUrl : ProjectProperties.wikiBaseUrl])
println html
new File(missingImagesDir, ProjectProperties.fileRefToCsvGenerator).text = html