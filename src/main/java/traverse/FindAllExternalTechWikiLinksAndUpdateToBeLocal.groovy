package traverse

import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import util.ProjectProperties

File baseDir = ProjectProperties.finalContentDir

baseDir.eachFileRecurse {file->
    if(file.name.endsWith("html") && !file.hidden){
        println "--------------- ${file.name} ---------------"
        def doc = Jsoup.parse(file.getText())

        doc.select("a").each{Element anchor->
            String attr = anchor.attr("href")
            if((attr.contains("mage:") || attr.contains("ile:"))){
                if(attr.contains("${ProjectProperties.wikiBaseUrl}/a/")){
                    def parts = attr.split(":")
                    if(parts.size() > 2){
                        println parts[2]
                        println ">>>> REPLACING THIS : ${attr} with ${parts[2]}"
                        anchor.attr("href", parts[2])
                        anchor.attr("data-original-href", attr)
                    }else{
                        println "!!!!!!!!!!!!!!!!!!! INVALID URL FOUND....you may want to fix it"
                    }
                }
            }
        }

        file.text = doc.html()
    }
}