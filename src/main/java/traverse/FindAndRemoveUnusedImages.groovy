package traverse

import org.jsoup.Jsoup
import util.ProjectProperties

File baseDir = ProjectProperties.finalContentDir

def images = []
def imagesInHtml = []
def imagesBeingUsed = []
def imagesNotFound = []

baseDir.eachFileRecurse {file->
    if(file.name.endsWith("html")){
        boolean modified = false
        def doc = Jsoup.parse(file.getText())
        doc.select("img").each{
            String srcAttr = it.attr("src")
            if(srcAttr.trim()){
                imagesInHtml << srcAttr.toLowerCase()
            }else{
                if("a".equals(it.parent().tagName())){
                    it.parent().remove()
                    modified = true;
                }else{
                    it.remove()
                    modified = true;
                }
            }
        }
        if(modified){
            println "!!!!! EMPTY IMAGE TAGS FOUND FOR FILE ${file.path}....REMOVING FROM DOCUMENT"
            file.text = doc.html()
        }
    }else{
        images << file.name.toLowerCase()
    }
}

imagesInHtml.each{htmlImage ->
    if(images.contains(htmlImage)){
        imagesBeingUsed << htmlImage
    }else{
        imagesNotFound << htmlImage
    }
}

println ">>>>> Images to be deleted since not used : "
images.removeAll(imagesBeingUsed)
println images.each{println it}


println "!!!!!!!!!!!!!!!!!!!!!!!!!!! Deleting unused images : "
images.each{
    new File(baseDir, it).delete()
}


println ">>>>> Images not found when searching all images locally : "
imagesNotFound.each{println it}