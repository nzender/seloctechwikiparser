package traverse
import download.ImageDownloader
import org.apache.commons.io.FilenameUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import resize.ImageResizer
import util.ProjectProperties

File baseDir = ProjectProperties.finalContentDir

boolean moreImagesFound = false
baseDir.eachFileRecurse {file->
    if(file.name.endsWith("html") && !file.hidden){
        println "--------------- ${file.name} ---------------"
        def doc = Jsoup.parse(file.getText())

        doc.select("img").each{Element image->
            String attr = image.attr("src")
            if(attr.contains("http://")){
                String fileName = FilenameUtils.getName(attr)
                boolean result = new ImageDownloader().download(attr, ProjectProperties.finalContentDir, fileName)

                if(result){
                    moreImagesFound = true
                    println ">>>> REPLACING THIS : ${attr} with ${fileName}"
                    image.attr("src", fileName)
                    image.attr("data-original-src", attr)

                    if("a" == image.parent().tagName()){
                        image.parent().attr("href", fileName)
                        image.parent().attr("data-original-href", attr)
                    }
                }else{
                    println "!!!!!!!! Image download failed removing the image and anchor tag (if it exists) on page ${file.name} for reference ${attr}"
                    if("a" == image.parent().tagName()){
                        image.parent().remove()
                    }else{
                        image.remove()
                    }
                }
            }
        }
        file.text = doc.html()
    }
}

if(moreImagesFound){
    new ImageResizer().run()
}