package convert

import org.apache.commons.io.FileExistsException
import org.apache.commons.io.FileUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import util.ProjectProperties

def removeBadChars = { String fileName ->
    fileName = fileName.replaceAll("&", "-")
    fileName = fileName.replaceAll("%26", "-")
    fileName = fileName.replaceAll("\\+", "-")
    fileName = fileName.replaceAll("%2b", "-")
    fileName = fileName.replaceAll("‘", "-")
    fileName = fileName.replaceAll("’", "-")
    fileName = fileName.replaceAll("%e2%80%98", "-")
    fileName = fileName.replaceAll("%e2%80%93", "-")
    fileName = fileName.replaceAll("%e2%80%99", "-")
    fileName = fileName.replaceAll("%e2%80%8e", "")
    //This one looks weird but it is a special long dash vs a normal dash. The long dash causes issues.
    fileName = fileName.replaceAll("–", "-")
    fileName = fileName.replaceAll("‘", "-")
    fileName
}

ProjectProperties.finalContentDir.eachFile { file ->
    if (!file.hidden && file.file) {
        if (file.name.endsWith("html")) {
            println ">>> Updating image references to lowercase ones in file with name ${file.name}"
            def doc = Jsoup.parse(file.getText())

            List<Element> images = doc.select("img")
            images.each { image ->
                String srcAttr = image.attr("src")
                image.attr("src", removeBadChars(srcAttr.toLowerCase()))

                Element imageParent = image.parent()
                if ("a".equals(imageParent.tagName())) {
                    String hrefAttr = imageParent.attr("href")
                    imageParent.attr("href", removeBadChars(hrefAttr.toLowerCase()))
                }
            }
            file.text = doc.html()
        } else {
            println ">>> Lowercasing file with name ${file.name}"
            File tempFile = File.createTempFile("seloc-image", ".img")
            tempFile.delete()
            FileUtils.moveFile(file, tempFile)
            File moveToLocation = new File(file.parentFile, removeBadChars(file.name.toLowerCase()))
            try{
                FileUtils.moveFile(tempFile, moveToLocation)
            }catch(FileExistsException e){
                println "!!!!!! File exists at location ${moveToLocation.path}"
            }
        }
    }
}
