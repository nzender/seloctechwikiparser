import convert.pages.CustomPageLogic
import info.bliki.wiki.filter.HTMLConverter
import info.bliki.wiki.model.WikiModel
import org.apache.commons.io.FileUtils
import templates.HtmlTemplates
import util.ProjectProperties
import wiki.SelocTechWikiModel

List<File> topLevelDirByType = ProjectProperties.topLevelDirectoriesForTypes

topLevelDirByType.each { topLevelDir ->
    println ">>>>>>>>>>>>> Processing all files of in topLevelDir ${topLevelDir.path} >>>>>>>>>>>>>>"

    //TODO This was originally a file since this was a manual step....now with the selenium scripts it is not needed as a file, need to refactor to not using this file
    File fileReferences = FileUtils.getFile(topLevelDir, ProjectProperties.outputDirPart,  "file_references.txt")
    fileReferences.parentFile.mkdirs()
    fileReferences.createNewFile()

    topLevelDir.eachFile { file ->
        if (file.isFile() && !file.isHidden() && file.name.endsWith("xml")) {
            println "------------- ${file.name} ----------------"
            def xml = new XmlSlurper().parse(file)
            xml.page.each {
                println it.title
                String value = it.revision.text.text()

                def matcher = (value =~ /(?i)(Image:[\w\s-._\(\)]*)|(File:[\w\s-._\(\)]*)/)
                (0..<matcher.getCount()).each { outer ->
                    Integer size = matcher[outer].size()
                    (0..<size).each { inner ->
                        def match = matcher[outer][inner]
                        if (match) {
                            def fileName = match.replace("|", "")
                            fileReferences << fileName + "\n"
                        }
                    }
                }

                def dirName = file.name.replace(".xml", "")
                def htmlDir = FileUtils.getFile(file.parentFile, dirName)
                if (!htmlDir.exists()) {
                    htmlDir.mkdir()
                }
                def htmlFile = FileUtils.getFile(htmlDir, "${it.id}.html")
                htmlFile.createNewFile()
                WikiModel model = new SelocTechWikiModel('${image}', ProjectProperties.wikiBaseUrl + '/a/${title}')

                value = CustomPageLogic.customProcessByFileName(htmlFile.name, value)

                value = value.replaceAll("&lt;gallery&gt;", "<gallery>");
                value = value.replaceAll("&lt;/gallery&gt;", "</gallery>");
                value = value.replaceAll("\\[img\\]", "[[Image:")
                value = value.replaceAll("\\[/img\\]", "]]")
                value = value.replaceAll("&amp;#", "&#");

                htmlFile.text = model.render(new HTMLConverter(), value);
            }
        }
    }

    def proc = """sort ${fileReferences.path}""".execute()
    fileReferences.text = proc.in.text

    proc = """uniq ${fileReferences.path}""".execute()
    fileReferences.text = proc.in.text

    def items = []
    fileReferences.text.eachLine {line ->
        items << line
    }
    def itemsHtml = HtmlTemplates.renderListOfItemsInChunksOfHtml(items)
    def html = HtmlTemplates.renderFileRefToUrlCsvGenerator([filesToLookup: itemsHtml, wikiBaseUrl : ProjectProperties.wikiBaseUrl])
    FileUtils.getFile(topLevelDir, ProjectProperties.outputDirPart, ProjectProperties.fileRefToCsvGenerator).text = html

    fileReferences.delete()
}