package convert.pages

import util.XmlUtils

public class CustomPageLogic {
    static def customLogic = [
        "2900.html" : {String text ->
            XmlUtils.partialXmlEscape(text)
        }
    ]

    public static String customProcessByFileName(String fileName, String contents){
        if(customLogic[fileName]){
            customLogic[fileName].call(contents)
        }else{
            contents
        }

    }
}
