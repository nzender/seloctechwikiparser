package templates
import groovy.text.SimpleTemplateEngine

class HtmlTemplates {

    public static final String fileRefToUrlCsvGeneratorHtml = '''
<html>
    <head>
    <style>
    #input{
        border:1px solid red;
        padding:10px;
    }
    #output{
        border:1px solid green;
        padding:10px;
    }
    </style>
</head>
    <body>
        <input type='button' id='doIt' value="Find Urls">
        <h3 id="doneYet">NOT STARTED</h3>
        <h2>CSV OUTPUT OF FILE REFERENCE -> URL</h2>
    <div id="output"></div>
        <h2>FILES TO LOOKUP URLS FOR</h2>
    <div id="input">
    ${filesToLookup}
    </div>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript">
        \\\$("#doIt").click(function () {
            var failed = false;

            var numberItems = \\\$("[name='items']").length
            var numberProcessed = 0

            \\\$("[name='items']").each(function(){
                var titles = [];
                \\\$(this).find("li").each(function(){
                    var title = \\\$(this).text().trim();
                    if(title.length > 0 && (title != "File:" || title != "Image:")){
                        titles.push(title)
                    }
                });

                if(failed === false){
                    \\\$.ajax({
                        async: false,
                        type: "GET",
                        url: "${wikiBaseUrl}/api.php?action=query&prop=imageinfo&iiprop=url&format=json&titles=" + titles.join("|"),
                        success: function(data){
                            for (var pageNum in data.query.pages) {
                                var pageData = data.query.pages[pageNum];
                                if(pageData.imageinfo){
                                    var url = pageData.imageinfo[0].url;

                                    var title = ""
                                    for(var i = 0, len = data.query.normalized.length; i < len; i ++){
                                        var map = data.query.normalized[i]
                                        if(map.to == pageData.title){
                                            title = map.from.split(":")[1]
                                            break;
                                        }
                                    }

                                    if(title === ""){
                                        title = pageData.title.split(":")[1]
                                    }

                                    numberProcessed ++;
                                    if(title != ""){
                                        \\\$("#output").append("<div>" + title + "," + url + "</div>");
                                    }
                                    if(numberProcessed == numberItems){
                                        \\\$("#doneYet").html("YES");
                                    }
                                }
                            }
                        },
                        error : function(){
                            failed = true;
                        },
                        dataType: "jsonp"
                    });
                }else{
                    \\$("#doneYet").html("FAILED");
                }
            });
        });
        </script>
    </body>
</html>
'''

    public static String renderFileRefToUrlCsvGenerator(params){
        def engine = new SimpleTemplateEngine()
        def template = engine.createTemplate(fileRefToUrlCsvGeneratorHtml).make(params)
        template.toString()
    }

    public static String renderListOfItemsInChunksOfHtml(items){
        StringBuilder sb = new StringBuilder()
        items.eachWithIndex { line, lineNumber ->
            if (lineNumber == 0) {
                sb.append "<ul name='items'>"
            }
            sb.append "<li>${line}</li>"
            if (lineNumber % 50 == 0) {
                sb.append "</ul>"
                sb.append "<ul name='items'>"
            }
        }
        sb.append "</ul>"
        sb.toString()
    }
}
