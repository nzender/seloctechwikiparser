package wiki.tag

import info.bliki.htmlcleaner.TagNode
import info.bliki.wiki.filter.ITextConverter
import info.bliki.wiki.model.IWikiModel
import info.bliki.wiki.tags.HTMLTag

class GalleryTag extends HTMLTag {

    GalleryTag(String name) {
        super("gallery")
    }

    @Override
    public void renderHTML(ITextConverter converter, Appendable buf, IWikiModel model) throws IOException {
        TagNode node = this;
        String name = node.getName();
        List<Object> children = node.getChildren();

        String content = children[0];

        content.eachLine { line ->
            def data = line.split("\\|")

            def image = data[0]
            if(image && (image.contains("mage:") || image.contains("ile:"))){
                def description = ""
                if(data.size() > 1){
                    description = data[1]
                }

                def renderedImage = model.render("[[${image}|120px]]")

                def newContent = """
                <div style="width: 155px">
                    <div style="width: 150px;text-align: center;border: 1px solid #ccc;background-color: #f9f9f9;margin: 2px;">
                        <div style="margin:27px auto;">
                            ${renderedImage}
                        </div>
                    </div>
                    <div class="overflow: hidden;font-size: 94%;padding: 2px 4px;word-wrap: break-word;">
                        <p>${description}</p>
                    </div>
                </div>
                """

                buf.append(newContent)
            }else{
                buf.append("<div>${image}</div>")
            }
        }
    }
}
