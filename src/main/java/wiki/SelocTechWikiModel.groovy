package wiki

import info.bliki.htmlcleaner.TagToken
import info.bliki.wiki.filter.Encoder
import info.bliki.wiki.filter.TemplateParser
import info.bliki.wiki.model.Configuration
import info.bliki.wiki.model.ImageFormat
import info.bliki.wiki.model.WikiModel
import info.bliki.wiki.tags.PTag
import util.ProjectProperties
import wiki.tag.GalleryTag

public class SelocTechWikiModel extends WikiModel {

    @Override
    public void substituteTemplateCall(String templateName, Map<String, String> parameterMap, Appendable writer) throws IOException {
        def xml = new XmlSlurper().parse(ProjectProperties.allTemplatesXmlFile);
        def template = xml.page.find{
            it.title.text() == ("Template:" + templateName.replace("_", " "))
        }

        StringBuilder templateBuffer = new StringBuilder();
        TemplateParser.parseRecursive(template.revision.text.text(), this, templateBuffer, false, true, true, parameterMap);
        writer.append(templateBuffer);
    }

    public SelocTechWikiModel(String imageBaseURL, String linkBaseURL) {
        super(imageBaseURL, linkBaseURL);
        this.getTokenMap().put("gallery", new GalleryTag())
    }
    public void parseInternalImageLink(String imageNamespace, String rawImageLink) {
        String imageSrc = getImageBaseURL();
        if (imageSrc != null) {
            String imageHref = getWikiBaseURL();
            ImageFormat imageFormat = ImageFormat.getImageFormat(rawImageLink, imageNamespace);

            String imageName = imageFormat.getFilename();
            if (imageName.endsWith(".svg")) {
                imageName += ".png";
            }
            imageName = Encoder.encodeUrl(imageName);
            if (replaceColon()) {
                imageName = imageName.replace(':', '/');
            }
            String link = imageFormat.getLink();
            if (link != null) {
                if (link.length() == 0) {
                    imageHref = "";
                } else {
                    String encodedTitle = encodeTitleToUrl(link, true);
                    imageHref = imageHref.replace('${title}', encodedTitle);
                }

            } else {
                if (replaceColon()) {
                    imageHref = imageHref.replace('${title}', imageNamespace + '/' + imageName);
                } else {
                    imageHref = imageHref.replace('${title}', imageNamespace + ':' + imageName);
                }
            }
            imageSrc = imageSrc.replace('${image}', imageName);
            String type = imageFormat.getType();
            TagToken tag = null;
            if ("thumb".equals(type) || "frame".equals(type)) {
                if (fTagStack.size() > 0) {
                    tag = peekNode();
                }
                reduceTokenStack(Configuration.HTML_DIV_OPEN);

            }
            appendInternalImageLink(imageHref, imageSrc, imageFormat);
            if (tag instanceof PTag) {
                pushNode(new PTag());
            }
        }
    }
}