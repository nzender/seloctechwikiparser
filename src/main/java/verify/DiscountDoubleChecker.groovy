package verify

import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import util.ProjectProperties

def projectErrors = []
if(ProjectProperties.finalContentDir.directorySize() > 80000000){
    projectErrors << "Overall final content dir is greater than 80mb...did everything get run correctly to optimize the content?!?!?"
}
if(projectErrors){
    println ">>>>> Issues found in project"
    projectErrors.each{println it}
}

ProjectProperties.finalContentDir.eachFile {file->

    def errors = []
    if(file.name.endsWith("html")){

        def doc = Jsoup.parse(file.text)
        doc.select("img").each{image->
            String src = image.attr("src")
            isStillPointingToExternalImage(src, errors)
            isPointingToSelecImageReference(src, errors)
            doesImageExistRelativeToTheFile(file, src, errors)
            verifyParentIfAnchorMatchesSameLocationAsImage(image, src, errors)
        }
        doc.select("a").each{anchor->
            String href = anchor.attr("href")
            isAnchorPointingToSelocReference(href, errors)
        }
        String textContents = doc.text()
        doesDocumentTextContainImageWikiTags(textContents, errors)
        doesDocumentTextContainFileWikiTags(textContents, errors)
        doesTextContainAnyEscapedHtml(textContents, errors)
    }

    if(errors){
        println ">>>>>> Found issues with ${file.name} for known bad things"
        errors.each{println it}
    }
}

private void verifyParentIfAnchorMatchesSameLocationAsImage(Element image, String src, errors) {
    Element parent = image.parent()
    if ("a" == parent.tagName()) {
        String href = parent.attr("href")
        if (href != src) {
            errors << "Images wrapped in anchors should generally match and point to the same location, anchor ${href} whereas image ${src}"
        }
    }
}

private void doesImageExistRelativeToTheFile(File file, String src, errors) {
    if (!new File(file.parentFile, src).exists()) {
        errors << "Image referenced at ${src} does not exist relative to the file"
    }
}

private void doesTextContainAnyEscapedHtml(String textContents, ArrayList errors) {
    if (textContents.contains("&lt;") || textContents.contains("&gt;")) {
        errors << "Escape characters found in html"
    }
}

private void doesDocumentTextContainFileWikiTags(String textContents, ArrayList errors) {
    if (textContents.find("[F|f]ile:")) {
        errors << "Rouge File: or file: text found in html text...usually means wiki markup is bad"
    }
}

private void doesDocumentTextContainImageWikiTags(String textContents, ArrayList errors) {
    if (textContents.find("\\[[I|i]mage:") || textContents.find("\\[img")) {
        errors << "Rouge [Image:, [image: or [img text found in html text...usually means wiki markup is bad"
    }
}

private void isAnchorPointingToSelocReference(String href, errors) {
    if (href.find("[I|i]mage:") || href.find("[F|f]ile:")) {
        errors << "Seloc image still referenced in anchor : ${href}"
    }
}

private void isPointingToSelecImageReference(String src, errors) {
    if (src.find("[I|i]mage:") || src.find("[F|f]ile:")) {
        errors << "Seloc image still being used and was not replaced : ${src}"
    }
}

private void isStillPointingToExternalImage(String src, errors) {
    if (src.startsWith("http")) {
        errors << "External reference found in image src : ${src}"
    }
}