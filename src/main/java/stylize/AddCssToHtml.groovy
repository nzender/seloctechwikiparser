package stylize

import org.jsoup.Jsoup
import util.ProjectProperties

ProjectProperties.finalContentDir.eachFileRecurse {file->
    if(file.name.endsWith("html") && !file.isHidden() && file.isFile()){
        def doc = Jsoup.parse(file.text)
        doc.head().append("<meta name=\"viewport\" content=\"width=device-width\" />")
        doc.head().append("""<style>
            pre {
             white-space: pre-wrap;       /* css-3 */
             white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
             white-space: -pre-wrap;      /* Opera 4-6 */
             white-space: -o-pre-wrap;    /* Opera 7 */
             word-wrap: break-word;       /* Internet Explorer 5.5+ */
            }
        </style>""")
        file.text = doc.html()
    }
}