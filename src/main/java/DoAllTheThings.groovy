if(args && args[0] == "removePreExistingOutput"){
    evaluate(new File("cleanup/Cleanup.groovy"))
}

evaluate(new File("browser/ExportTemplates.groovy"))
evaluate(new File("browser/ExportXmls.groovy"))

evaluate(new File("convert/TechWikiToHtml.groovy"))
new GroovyShell(new Binding(["cartypes", "categories"] as String[])).evaluate(new File("browser/ImageLinkToUrlRealUrl.groovy"))

evaluate(new File("download/ImageDownloader.groovy"))
evaluate(new File("traverse/TraverseHtmlAndFindMissingReferences.groovy"))
new GroovyShell(new Binding(["missing"] as String[])).evaluate(new File("browser/ImageLinkToUrlRealUrl.groovy"))

evaluate(new File("download/ImageDownloader.groovy"))

evaluate(new File("combine/CombineAllImages.groovy"))
evaluate(new File("combine/CombineAllHtml.groovy"))

evaluate(new File("resize/ImageResizer.groovy"))

evaluate(new File("traverse/FindAllExternalTechWikiLinksAndUpdateToBeLocal.groovy"))
evaluate(new File("traverse/FindAllExternalImagesAndUpdateToBeLocal.groovy"))
evaluate(new File("convert/LowercaseAllImagesAndUpdateHtmlReferences.groovy"))

evaluate(new File("stylize/AddCssToHtml.groovy"))
evaluate(new File("resize/CompressHtmlFiles.groovy"))

evaluate(new File("android/navigation/GenerateAndroidNavigationFromWikiXml.groovy"))

evaluate(new File("traverse/FindAndRemoveUnusedImages.groovy"))
evaluate(new File("resize/ImageOptimization.groovy"))

println("---------- MANUAL FIXES REQUIRED -----------")
evaluate(new File("verify/DiscountDoubleChecker.groovy"))
println("---------- END MANUAL FIXES REQUIRED -----------")

println("All processing completed")
println("\t Fix any errors reported above from the DiscountDoubleChecker script")

System.exit(0)