<table id="toc" class="toc" summary="Contents">
<tr>
<td>
<div id="toctitle">
<h2>Contents</h2>
</div>
<ul>
<li class="toclevel-1"><a href="#Introduction">Introduction</a>
<ul>
<li class="toclevel-2"><a href="#Manual_gearbox_information">Manual gearbox information</a>
</li>
<li class="toclevel-2"><a href="#Automatic_gearbox_information">Automatic gearbox information</a>
<ul>
<li class="toclevel-3"><a href="#Drive">Drive</a>
</li>
<li class="toclevel-3"><a href="#Sport">Sport</a>
</li>
<li class="toclevel-3"><a href="#Paddle">Paddle</a>
</li>
<li class="toclevel-3"><a href="#Shortcuts">Shortcuts</a>
</li>
</ul>
</li>
<li class="toclevel-2"><a href="#Other_information">Other information</a>
</li>
</ul>
</li>
<li class="toclevel-1"><a href="#Gear_Ratios">Gear Ratios</a>
</li>
<li class="toclevel-1"><a href="#Maximum_Speed_per_gear">Maximum Speed per gear</a>
</li>
<li class="toclevel-1"><a href="#Emissions">Emissions</a>
</li>
<li class="toclevel-1"><a href="#References">References</a>
</li>
</ul></td></tr></table><hr/>
<h1><span class="mw-headline" id="Introduction">Introduction</span></h1>

<p>The standard model <a href="http://wiki.seloc.org/a/Models_-_Evora#Evora" title="Models - Evora">Lotus Evora</a> comes with the choice of 3 gearboxes, the standard 6-speed manual, the Close Ratio Gearbox - again a 6-speed manual but with a closer ratio gear set - and the Lotus IPS 6-speed automatic, making the car an Evora IPS. The standard ratio box was dropped for <a href="http://wiki.seloc.org/a/Models_-_Evora#MY12_Updates" title="Models - Evora">MY12</a>, making the Close Ratio Gearbox the standard fit for normally aspirated cars.</p>
<p>The supercharged <a href="http://wiki.seloc.org/a/Models_-_Evora#Evora_S" title="Models - Evora">Evora S</a> dropped the standard ratio 6-speed gearbox and was only offered with the Close Ratio Gearbox initially, with the IPS box being added as an option to create the Evora S IPS from MY12 onwards. </p>
<p>The Close Ratio Gearbox is the only option available for the <a href="http://wiki.seloc.org/a/Models_-_Exige_V6#Exige_S_.282012.29" title="Models - Exige V6">2012 Exige S</a> and other road going variants, although some race variants of the Exige and Evora such as the <a href="http://wiki.seloc.org/a/Models_-_Exige_V6#Exige_V6_Cup_R" title="Models - Exige V6">Exige V6 Cup R</a> and <a href="http://wiki.seloc.org/a/Models_-_Evora#Evora_GT4" title="Models - Evora">Evora GT4</a> are available with a sequential gearbox, with optional paddle-shift.</p>
<p>All of the road car gearboxes are Toyota sourced, with the IPS featuring Lotus&#39;s own electronic controls to make it more suitable for a sporting application, while the sequential box is supplied by X-Trac.</p>
<h2><span class="mw-headline" id="Manual_gearbox_information">Manual gearbox information</span></h2>

<p>For the manual gearboxes this is the Toyota EA60 gearbox, as fitted to the Toyota Auris 2.0 D-4D and 2.2 D-CAT models.<sup><a href="#ref1">1</a></sup> These are diesel engines, and the standard ratios are as they in the Auris, possibly explaining why they are often seen as rather long. The closer ratio gear set appears to be a bespoke set of ratios unique to the Evora and Evora S.</p>
<p>An adaptor plate is used to allow the engine and gearbox to be joined together, and the starter motor is relocated compared to the Toyota application.</p>
<p>For the manual gearboxes Lotus list two final drive ratios, one for 1st through 4th gears and another slightly shorter ratio for 5th and 6th.<sup><a href="#ref2">2</a></sup></p>
<h2><span class="mw-headline" id="Automatic_gearbox_information">Automatic gearbox information</span></h2>

<p>The Evora IPS and S IPS use the Toyota U660E gearbox<sup><a href="#ref3">3</a></sup> as used by Toyota in conjunction with the 2GR-FE engine in several vehicles.</p>
<p>There are 3 modes of operation for the IPS gearbox: Drive, Sport, and paddle over-ride.<sup><a href="#ref4">4</a></sup><br /></p>
<h3><span class="mw-headline" id="Drive">Drive</span></h3>
<p>Drive is biassed towards fuel economy, generally keeping the engine below 2000rpm.</p>
<h3><span class="mw-headline" id="Sport">Sport</span></h3>
<p>Sport mode adds throttle blips on downshifts and generally runs the engine at higher revs to improve responsiveness.</p>
<h3><span class="mw-headline" id="Paddle">Paddle</span></h3>
<p>Paddle mode is activated by pulling on one of the paddles in either Drive or Sport mode, and allows the driver to change gear with the paddles mounted behind the wheel.</p>
<p>In Drive mode the car will return to Automatic operation if the paddles are not used for 10 seconds, and will shift up when the rev limiter is hit.</p>

<p>In Sport mode the gearbox will not shift up automatically even if the rev limit is reached. The original gearbox software would return to Automatic operation if the paddles were not used for 30 seconds in Sport mode, but the latest software updates, carried out when the car is taken for a main dealer service, see the car remain in Sport Manual mode indefinitely until the Drive button is pressed or Sport mode is disabled.<sup><a href="#ref5">5</a></sup></p>

<p>Unlike the manuals, cars fitted with the IPS gearbox do not have the engines rev limit increased to 7000rpm when in Sport Mode, as this would damage the gearbox.<sup><a href="#ref6">6</a></sup></p>

<h3><span class="mw-headline" id="Shortcuts">Shortcuts</span></h3>

<p>The following paddle presses perform certain actions<sup id="_ref-1" class="reference"><a href="#_note-1" title="">[1]</a></sup>:
</p>
<ul>
<li>Pulling and holding both paddles together for a couple seconds will select Neutral.</li>
<li>When in manual mode, holding the Up paddle for a couple seconds will revert to Drive.</li>
</ul>

<h2><span class="mw-headline" id="Other_information">Other information</span></h2>

<p>The IPS gearbox was introduced as an option for the Evora in September 2010 and the Evora S in September 2011.</p>
<p>All Evora S&#39;s come with the close ratio Sport Gearbox fitted as standard. For the standard Evora the Sport Gearbox was added as standard equipment for the MY12 cars onwards.</p>
<h1><span class="mw-headline" id="Gear_Ratios">Gear Ratios</span></h1>


<div style="page-break-inside: avoid;">
<table border="1" cellpadding="5" cellspacing="0">
<tr>
<th>Gearbox </th>
<th>1 </th>
<th>2 </th>
<th>3 </th>
<th>4 </th>
<th>5 </th>
<th>6 </th>
<th>Reverse</th>
<th>Final Drive 1st - 4th </th>
<th>Final Drive 5th - 6th</th></tr>
<tr>
<td><b>Standard</b>  </td>
<td>3.538 </td>
<td>1.913 </td>
<td>1.218 </td>
<td>0.86 </td>
<td>0.79 </td>
<td>0.638 </td>
<td>3.831 </td>
<td>3.777 </td>
<td>3.238</td></tr>
<tr>
<td><b>Close Ratio</b>  </td>
<td>3.538 </td>
<td>1.913 </td>
<td>1.407 </td>
<td>1.091 </td>
<td>0.9697 </td>
<td>0.8611 </td>
<td>3.831 </td>
<td>3.777 </td>
<td>3.238</td></tr>
<tr>
<td><b>IPS</b>  </td>
<td>3.300 </td>
<td>1.900 </td>
<td>1.420 </td>
<td>1.000 </td>
<td>0.713 </td>
<td>0.608 </td>
<td>4.148 </td>
<td>3.935<sup><a href="#ref7">7</a></sup> </td>
<td>3.935</td></tr></table></div>

<h1><span class="mw-headline" id="Maximum_Speed_per_gear">Maximum Speed per gear</span></h1>

<p>Speeds displayed at 6400rpm and 7000rpm in brackets for Sport Mode.</p>

<div style="page-break-inside: avoid;">
<table border="1" cellpadding="5" cellspacing="0">
<tr>
<th>Gearbox </th>
<th>1 </th>
<th>2 </th>
<th>3 </th>
<th>4 </th>
<th>5 </th>
<th>6</th></tr>
<tr>
<td><b>Standard</b>  </td>
<td>37.09mph (40.57mph) </td>
<td>68.60mph (75.03mph) </td>
<td>107.75mph (117.85mph) </td>
<td>152.60mph (166.90mph) </td>
<td>193.77mph (211.94mph) </td>
<td>239.94mph (262.43mph)</td></tr>
<tr>
<td><b>Close Ratio</b>  </td>
<td>37.09mph (40.57mph) </td>
<td>68.60mph (75.03mph) </td>
<td>93.27mph (102.02mph) </td>
<td>120.29mph (131.56mph) </td>
<td>157.86mph (172.66mph) </td>
<td>177.77mph (194.44mph)</td></tr>
<tr>
<td><b>IPS</b>  </td>
<td>38.17mph </td>
<td>66.30mph </td>
<td>88.71mph </td>
<td>125.96mph </td>
<td>176.71mph </td>
<td>207.18mph</td></tr></table></div>

<h1><span class="mw-headline" id="Emissions">Emissions</span></h1>

<p>The official CO<sub>2</sub> emission and fuel consumption ratings for the Evora vary depending on the gearbox equipped to the car.</p>

<div style="page-break-inside: avoid;">
<table border="1" cellpadding="5" cellspacing="0">
<tr>
<th />
<th>Evora MY10 </th>
<th>Evora MY11</th>
<th>Evora Sport Gearbox </th>
<th>Evora IPS </th>
<th>Evora S </th>
<th>Evora S IPS</th></tr>
<tr>
<td>Fuel Consumption - Urban </td>
<td>22.8mpg (12.4 l/100km) </td>
<td>23.4mpg (12.1 l/100km) </td>
<td>22.1mpg (12.8 l/100km) </td>
<td>22.4mpg (12.6 l/100km) </td>
<td>19.4mpg (14.6 l/100km) </td>
<td>19.6mpg (14.4 l/100km)</td></tr>
<tr>
<td>Fuel Consumption - Extra Urban </td>
<td>43.5mpg (6.5 l/100km)</td>
<td>44.8mpg (6.3 l/100km) </td>
<td>40.4mpg (7.0 l/100km) </td>
<td>44.8mpg (6.3 l/100km) </td>
<td>37.2mpg (7.6 l/100km) </td>
<td>40.8mpg (6.9 l/100km)</td></tr>
<tr>
<td>Fuel Consumption - Combined </td>
<td>32.5mpg (8.7 l/100km) </td>
<td>33.2 (8.5 l/100km) </td>
<td>31.0mpg (9.1 l/100km) </td>
<td>33.2mpg (8.5 l/100km) </td>
<td>27.2mpg (10.2 l/100km) </td>
<td>29.3mpg (9.7 l/100km)</td></tr>
<tr>
<td>CO<sub>2</sub> Emissions - Urban </td>
<td>- </td>
<td>285g/km </td>
<td>301g/km </td>
<td>297g/km </td>
<td>341g/km </td>
<td></td></tr>
<tr>
<td>CO<sub>2</sub> Emissions - Extra Urban </td>
<td>- </td>
<td>149g/km </td>
<td>165g/km </td>
<td>156g/km </td>
<td>179g/km </td>
<td></td></tr>
<tr>
<td>CO<sub>2</sub> Emissions - Combined </td>
<td>205g/km </td>
<td>199g/km </td>
<td>215g/km </td>
<td>208g/km </td>
<td>239g/km </td>
<td>224g/km</td></tr></table></div>

<h1><span class="mw-headline" id="References">References</span></h1><ol class="references">
<li id="_note-1"><b><a href="#_ref-1" title="">&#8593;</a></b> <a class="external text" href="http://www.pistonheads.com/gassing/topic.asp?h=0&#38;f=213&#38;t=1292961&#38;nmt=" rel="nofollow">PistonHeads &#62; Gassing Station &#62; Lotus &#62; Evora &#62; IPS shortcuts...</a></li>
</ol>
<div id="ref1"><sup>1</sup><a class="external text" href="http://www.wess.lv/toyota/Public/upload/toyotaspecs/Auris_specs_eng.pdf" rel="nofollow">Toyota Auris Specification</a></div>
<div id="ref2"><sup>2</sup> <a class="external text" href="http://www.lotuscars.com/en/lotus-evora-specification" rel="nofollow">Lotus Cars: Lotus Evora Specification</a></div>
<div id="ref3"><sup>3</sup> <a class="external text" href="http://en.wikipedia.org/wiki/Toyota_U_transmission#U660E" rel="nofollow">- Wikipedia - Toyota U transmissions</a></div>
<div id="ref4"><sup>4</sup> <a class="external text" href="http://www.pistonheads.com/news/default.asp?storyId=23760" rel="nofollow">Pistonheads DRIVEN: LOTUS EVORA IPS</a></div>
<div id="ref5"><sup>5</sup> <a class="external text" href="http://forums.seloc.org/viewthread.php?tid=307846&#38;page=1#pid5232361" rel="nofollow">SELOC Forums - Evora IPS</a></div>
<div id="ref6"><sup>6</sup> <a class="external text" href="http://www.lotussilverstone.co.uk/new-cars/view/74/Lotus-Evora-IPS" rel="nofollow">- Lotus Silverstone - Lotus Evora IPS</a></div>
<div id="ref7"><sup>7</sup> <a class="external text" href="http://www.insideline.com/toyota/sienna/2011/2011-toyota-sienna-vs-2010-honda-odyssey-minivan-comparison.html" rel="nofollow">Edmunds Inside Line - 2011 Toyota Sienna vs. 2010 Honda Odyssey Minivan Comparison (This car appears to have the same gearbox and ratios as the Evora IPS)</a></div>

<p>

</p>